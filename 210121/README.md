# Quand ?
Jeudi 21 Janvier 2020 - 14h-18h

# Programme
| Horaire | Description | Intervenants |
| -------- | -------- | -------- |
| 14h-14h30   | [Introduction + Tour de table virtuel](https://pad.interhop.org/p/r1hDJ0rJO#/)  | Antoine Lamer, Adrien Parrot     |
| 14h30-14h50    | [Présentation des projets](https://framagit.org/interhop/omop/journees_omop_france_interchu/-/blob/main/21%20janvier/Pr%C3%A9sentations/20210121-interhop-presentation-ee.pdf)     | Easter-Eggs     |
| 14h50-15h10 | Outil de visualisation du parcours Patient | Fabio Boudis (CHU Lille) |
| 15h10-15h30    | [Etat des lieux EDS Toulouse](https://framagit.org/interhop/omop/journees_omop_france_interchu/-/blob/main/21%20janvier/Pr%C3%A9sentations/interhop_21012021.pdf)     | Prosper Burq (CHU Toulouse)     |
| 15h30-15h50    | [Génération de jeux de donnees](https://pad.interhop.org/p/SJ1lcKH1d)     | Dinh Phong Nguyen (APHP)     |
| 15h50-16h10    | [Alignement terminologique - Susana](https://pad.interhop.org/p/BysYaRL1_#/)    | Adrien Parrot, Nicolas Paris (InterHop)     |
| 16h10-16h30| [Mapping des médicaments français vers RxNorm](https://framagit.org/interhop/omop/journees_omop_france_interchu/-/blob/main/21%20janvier/Pr%C3%A9sentations/RxNorm_Sebastien_Cossin.pdf) | Sébastien Cossin (CHU Bordeaux) |
| 16h30-16h50 | Retour d'expérience à Marseille | François Antonini, Clément Fraboulet (APHM) |
| 16h50-17h10 | [Calcul décentralisé, opportunité pour les modèles communs](https://framagit.org/interhop/omop/journees_omop_france_interchu/-/blob/main/21%20janvier/Pr%C3%A9sentations/omop_anesthesia_210121.pdf) | Antoine Lamer (CHU Lille) |
| 17h10-17h30    | [Intégration des données peropératoires + calcul d'agrégats](https://framagit.org/interhop/omop/journees_omop_france_interchu/-/blob/main/21%20janvier/Pr%C3%A9sentations/presentation_feature_210121.pdf) | Antoine Lamer (CHU Lille, ULR2694, InterHop)  |
| 17h30-17h40 | Clôture | Adrien Parrot, Antoine Lamer |

# Où ?

Nous utilisons la solution de vidéo conférence BigBlueButton, opensource et protectrice des données personnelles car hébergée chez Octopuce, société de droit français (avec des serveurs en France) : https://www.octopuce.fr/mentions-legales/
