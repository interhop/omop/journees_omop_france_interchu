# Wébinaire InterCHU 15/06/2021 - Interopérabilité des médicaments

 - 9h00 - 9h30 - Antoine LAMER (CHU Lille, InterHop) : Présentation des journées InterCHU
 - 9h30 -10h00 - Adrien PARROT, Nicolas PARIS (InterHop) : [Comment collaborer sur les terminologies avec Susana ?](https://peertube.interhop.org/videos/watch/f5f324ff-07ab-4698-904c-eb5dfaedeff1)
 - 10h00 - 10h30 - Florent Desgrippes (APHP, InterHop) : L’interopérabilité et le médicament
 - 10h30 - 11h00 - Julien Dubiel (APHP) : [profiling des ressources FHIR et médicament](https://peertube.interhop.org/videos/watch/44994dfb-0596-47f1-b5c9-f6112cb56e96)
 - 11h00 - 11h30 - Sébastien Cossin (CHU Bordeaux) : mapping sémantique et médicament
 - 11h30 - 12h - Jean-François Laurent (CNAM, Thesorimed) : présentation du référentiel Thésorimed 

